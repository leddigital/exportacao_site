/* Add here all your JS customizations */

$(document).ready(function(){
    var container = $('#container');
    
    if(container.val() == "pallet") {
        $('#qtd_caixa').prop('disabled', true);
        $('#cla').prop('disabled', true);
        $('#m3_caixa').prop('disabled', true);
        $('#peso_caixa').prop('disabled', true);
    }
    
    container.on('change', function(){
        if(container.val() == "pallet") {
            $('#qtd_caixa').prop('disabled', true);
            $('#cla').prop('disabled', true);
            $('#m3_caixa').prop('disabled', true);
            $('#peso_caixa').prop('disabled', true);
        } else {
            $('#qtd_caixa').prop('disabled', false);
            $('#cla').prop('disabled', false);
            $('#m3_caixa').prop('disabled', false);
            $('#peso_caixa').prop('disabled', false);
        }
    });
});