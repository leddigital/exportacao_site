-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Tempo de geração: 28/11/2019 às 19:10
-- Versão do servidor: 5.6.41-84.1
-- Versão do PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `agenci62_kinino`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `banners`
--

CREATE TABLE `banners` (
  `id` int(11) NOT NULL,
  `image` text NOT NULL,
  `title` varchar(255) NOT NULL,
  `info1` text,
  `info2` text,
  `info3` text,
  `link1` text,
  `link2` text,
  `link3` text,
  `path` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `order` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `banners`
--

INSERT INTO `banners` (`id`, `image`, `title`, `info1`, `info2`, `info3`, `link1`, `link2`, `link3`, `path`, `created_at`, `updated_at`, `status`, `order`) VALUES
(1, 'banner_5dde94cf418e6.jpeg', 'Banner 1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-27 15:22:55', '2019-11-27 15:22:55', NULL, NULL),
(2, 'banner_5dde953c3d956.jpeg', 'Banner 2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-27 15:24:44', '2019-11-27 15:24:44', NULL, NULL),
(3, 'banner_5dde954dc2100.jpeg', 'Banner 3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-27 15:25:01', '2019-11-27 15:25:01', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura para tabela `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `short_description` text,
  `image` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `categories`
--

INSERT INTO `categories` (`id`, `title`, `url`, `short_description`, `image`) VALUES
(8, 'sdsdsds', 'sdsdsds', 'dsdsdsdsd', 'categoria_5ddfe82712269.jpeg'),
(9, 'asdasd', 'asdasd', 'asdasda', 'categoria_5ddfe8344acd7.jpeg'),
(11, 'asd asdsa', 'asd-asdsa', 'as das das as dsa dsa', 'categoria_5ddfe84e3b016.jpeg'),
(12, 'asd asd asd as', 'asd-asd-asd-as', 'asd as das as', 'categoria_5ddfe85b64a12.jpeg'),
(13, 'a sda sda sd', 'a-sda-sda-sd', 'as dasd as dsa', 'categoria_5ddfe86761e5a.jpeg');

-- --------------------------------------------------------

--
-- Estrutura para tabela `categories_has_contents`
--

CREATE TABLE `categories_has_contents` (
  `categories_id` int(11) NOT NULL,
  `contents_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `categories_has_contents`
--

INSERT INTO `categories_has_contents` (`categories_id`, `contents_id`) VALUES
(12, 3);

-- --------------------------------------------------------

--
-- Estrutura para tabela `contents`
--

CREATE TABLE `contents` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `type` varchar(45) NOT NULL COMMENT 'post,news,page,page-name',
  `url` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `validade` varchar(45) NOT NULL DEFAULT '0',
  `peso_unitario` varchar(45) NOT NULL DEFAULT '0',
  `ncm` varchar(45) NOT NULL DEFAULT '0',
  `qtd_caixa` varchar(45) NOT NULL DEFAULT '0',
  `cla` varchar(45) NOT NULL DEFAULT '0',
  `m3_caixa` varchar(45) NOT NULL DEFAULT '0',
  `peso_caixa` varchar(45) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `sku` int(11) NOT NULL DEFAULT '0',
  `container` varchar(45) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `contents`
--

INSERT INTO `contents` (`id`, `title`, `type`, `url`, `image`, `validade`, `peso_unitario`, `ncm`, `qtd_caixa`, `cla`, `m3_caixa`, `peso_caixa`, `created_at`, `updated_at`, `sku`, `container`) VALUES
(3, 'BATATA PALHA TRADICIONAL', 'produto', 'batata-palha-tradicional', 'produto_5de02f377cedc.jpeg', '6', '120G', '20052000', '26X120G', '42,0 X 26,0 X 26,5', '0,0289', '3,48', '2019-11-28 20:33:59', '2019-11-28 20:33:59', 273, 'caixa');

-- --------------------------------------------------------

--
-- Estrutura para tabela `contents_has_contents`
--

CREATE TABLE `contents_has_contents` (
  `contents_id` int(11) NOT NULL,
  `contents_child_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `contents_images`
--

CREATE TABLE `contents_images` (
  `id` int(11) NOT NULL,
  `description` text,
  `type` varchar(45) DEFAULT NULL,
  `order` varchar(45) DEFAULT NULL,
  `image` text,
  `contents_id` int(11) NOT NULL,
  `path` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `informations`
--

CREATE TABLE `informations` (
  `id` int(11) NOT NULL,
  `address` text,
  `number` varchar(45) DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `zipcode` varchar(45) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `whatsapp` varchar(45) DEFAULT NULL,
  `instagram` text,
  `facebook` text,
  `linkedin` text,
  `twitter` text,
  `pinterest` text,
  `phone1` varchar(45) DEFAULT NULL,
  `email` text,
  `phone2` varchar(45) DEFAULT NULL,
  `texto_1` text,
  `texto_2` text,
  `valor_1` text,
  `valor_2` text,
  `valor_3` text,
  `valor_4` text,
  `valor_5` text,
  `valor_texto_1` text,
  `valor_texto_2` text,
  `valor_texto_3` text,
  `valor_texto_4` text,
  `valor_texto_5` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura para tabela `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Fazendo dump de dados para tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, 'leddigital', 'digital@agencialed.com.br', NULL, '$2y$10$6NsTVvIenPGcOpcmLpjxIeS5p5Q4oTK59ThKMj9kuCnRgIwisgUgq', 'fRxlJZDRSNlkoUzwym7xsJ9XUQvn5Qe9E5x951FoO5Wlqxy61qpoTYDWuXp8', '2019-09-26 20:22:30', '2019-09-26 20:22:30');

--
-- Índices de tabelas apagadas
--

--
-- Índices de tabela `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `categories_has_contents`
--
ALTER TABLE `categories_has_contents`
  ADD PRIMARY KEY (`categories_id`,`contents_id`),
  ADD KEY `fk_categories_has_contents_contents1_idx` (`contents_id`),
  ADD KEY `fk_categories_has_contents_categories1_idx` (`categories_id`);

--
-- Índices de tabela `contents`
--
ALTER TABLE `contents`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `url_UNIQUE` (`url`);

--
-- Índices de tabela `contents_has_contents`
--
ALTER TABLE `contents_has_contents`
  ADD PRIMARY KEY (`contents_id`,`contents_child_id`),
  ADD KEY `fk_contents_has_contents_contents2_idx` (`contents_child_id`),
  ADD KEY `fk_contents_has_contents_contents1_idx` (`contents_id`);

--
-- Índices de tabela `contents_images`
--
ALTER TABLE `contents_images`
  ADD PRIMARY KEY (`id`,`contents_id`),
  ADD KEY `fk_contents_images_contents_idx` (`contents_id`);

--
-- Índices de tabela `informations`
--
ALTER TABLE `informations`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Índices de tabela `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Índices de tabela `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de tabelas apagadas
--

--
-- AUTO_INCREMENT de tabela `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de tabela `contents`
--
ALTER TABLE `contents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de tabela `contents_images`
--
ALTER TABLE `contents_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `informations`
--
ALTER TABLE `informations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restrições para dumps de tabelas
--

--
-- Restrições para tabelas `categories_has_contents`
--
ALTER TABLE `categories_has_contents`
  ADD CONSTRAINT `fk_categories_has_contents_categories1` FOREIGN KEY (`categories_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_categories_has_contents_contents1` FOREIGN KEY (`contents_id`) REFERENCES `contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para tabelas `contents_has_contents`
--
ALTER TABLE `contents_has_contents`
  ADD CONSTRAINT `fk_contents_has_contents_contents1` FOREIGN KEY (`contents_id`) REFERENCES `contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_contents_has_contents_contents2` FOREIGN KEY (`contents_child_id`) REFERENCES `contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Restrições para tabelas `contents_images`
--
ALTER TABLE `contents_images`
  ADD CONSTRAINT `fk_contents_images_contents` FOREIGN KEY (`contents_id`) REFERENCES `contents` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
