<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'title',
        'type',
        'url',
        'image',
        'short_description',
        'lang'
    ];

    public function contents(){
        return $this->belongsToMany('App\Contents','categories_has_contents');
    }
}
