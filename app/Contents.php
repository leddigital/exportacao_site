<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Contents extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'title',
        'description',
        'short_description',
        'content',
        'image',
        'type',
        'url',
        'validade',
        'peso_unitario',
        'ncm',
        'qtd_caixa',
        'cla',
        'm3_caixa',
        'peso_caixa',
        'sku',
        'container',
        'lang',
        'conjunto'
    ];

    public $rules = [
        'title' => 'required',
        'type'=>'required',
    ];

    public function images(){
        return $this->hasMany('App\ContentsImages');
    }

    public function categories(){
        return $this->belongsToMany('App\Categories','categories_has_contents');
    }
}
