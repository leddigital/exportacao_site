<?php

namespace App\Http\Controllers;

use App\Mail\SendMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    public function email(Request $req)
    {
        $form = $req->all();
        Mail::to(['edinaldo@agencialed.com.br'])
            ->send(new SendMail($form, $form['assunto']));

        if (Mail::failures()) {
            echo '0';
            exit;
        }
        echo '1';
    }

}
