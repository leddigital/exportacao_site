<?php

namespace App\Http\Controllers;

use App\Categories;
use Illuminate\Http\Request;

class InsertCategories extends Controller
{

    protected $model;

    public function __construct()
    {
        $this->model = new Categories();
    }

    public function save () {

        $handle = fopen('categorias.csv', 'r');
        $header = true;

        $cont = 1;

        while ($data = fgetcsv($handle, 10000, ";")) {

            if ($header) $header = false;
            else {
                $categoria = [
                    'title' => $data[0],
                    'url' => $this->url_verify($data[0], $this->model),
                    'short_description' => $data[1],
                    'image' => $data[2],
                    'lang' => $data[3]
                ];

                if(!file_exists(public_path() . "/img/images/cat/" . $categoria['image'])) {
                    echo "<h3>Imagem " . $categoria['image'] . " não existe</h3><br>";
                }

                $inserted = $this->model->create($categoria);

                if ($inserted) echo $cont . " - <strong>" . $categoria['title'] . "</strong> adicionada com sucesso! <br>";
                else echo $cont . " - Falha ao adicionar <strong>". $categoria['title'] . "</strong><br>";

                $cont++;
            }

        }

    }
}
