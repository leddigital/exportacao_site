<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Contents;
use App\ContentsImages;
use Illuminate\Http\Request;

class ContentsController extends Controller
{
    protected $model;

    public function __construct()
    {
        $this->model = new Contents();
    }

    public function index(Request $request) {
        //Pega o tipo de conteudo que vou gerenciar
        $_type = $request->route('_type');
        return view('admin.content.' . $_type . '.index', ['_type' => $_type]);
    }

    //Listar todas as categorias
    public function readAll(Request $request) {
        $type = $request->route('_type');
        $collection = $this->model->where('type', '=', $type)->get()->all();

        $data['data'] = $collection;
        echo json_encode($data);
    }

    public function form(Request $request) {

        $id = $request->route('id');
        $_type = $request->route('_type');

        if (isset($id) and ($id != "")) {

            $entity = $this->model->with(['categories'])->find($id);
            $gallery = $entity->images()->get();

            $categories = Categories::with(['contents' => function ($q) use ($id) {
                $q->where('id', $id);
            }])->orderBy('title', 'ASC')
                ->get();

            return view('admin.content.' . $_type . '.form', [
                'entity' => $entity,
                'categories' => $categories,
                'gallery' => $gallery,
                '_type' => $_type,
            ]);

        } else {
            $categories = Categories::orderBy('title', 'ASC')->get();
            return view('admin.content.' . $_type . '.form', ['categories' => $categories, '_type' => $_type]);
        }
    }

    public function save(Request $request) {

        $folder = public_path() . '/img/';
        $form = $request->all();
        $id = $request->route('id');

        // Verifica se a pasta de conteudo já existe, se nao existir, a cria.
        if (!file_exists($folder)) {
            mkdir($folder, 0777);
        }

        // Se o tipo for pallet, zerar os valores
        if(isset($form['container']) && $form['container'] == "pallet") {
            $form['qtd_caixa'] ="";
            $form['cla'] ="";
            $form['m3_caixa'] ="";
            $form['peso_caixa'] ="";
        }

        // Inserir novo registro
        if (!isset($id) and $id == "") {

            // Salva a img
            $form['image'] = $this->saveImg($form['base64'], 'produto_', '/img/products/');

            for ($i=0; $i < count($form['title']); $i++) {

                if($i == 0){

                    $product[$i]['title'] = $form['title'][$i];
                    $product[$i]['sku'] = $form['sku'];
                    $product[$i]['type'] = $form['type'];
                    $product[$i]['url'] = $this->url_verify($form['title'][$i], $this->model);
                    // CONJUNTO
                    $product[$i]['image'] = $form['image'];
                    $product[$i]['validade'] = $form['validade'];
                    $product[$i]['peso_unitario'] = $form['peso_unitario'];
                    $product[$i]['ncm'] = $form['ncm'];
                    $product[$i]['qtd_caixa'] = $form['qtd_caixa'];
                    $product[$i]['cla'] = $form['cla'];
                    $product[$i]['m3_caixa'] = $form['m3_caixa'];
                    $product[$i]['peso_caixa'] = $form['peso_caixa'];
                    $product[$i]['container'] = $form['container'];
                    $product[$i]['lang'] = 'en';

                } else if ($i == 1) {

                    $product[$i]['title'] = $form['title'][$i];
                    $product[$i]['sku'] = $form['sku'];
                    $product[$i]['type'] = $form['type'];
                    $product[$i]['url'] = $this->url_verify($form['title'][$i], $this->model);
                    // CONJUNTO
                    $product[$i]['image'] = $form['image'];
                    $product[$i]['validade'] = $form['validade'];
                    $product[$i]['peso_unitario'] = $form['peso_unitario'];
                    $product[$i]['ncm'] = $form['ncm'];
                    $product[$i]['qtd_caixa'] = $form['qtd_caixa'];
                    $product[$i]['cla'] = $form['cla'];
                    $product[$i]['m3_caixa'] = $form['m3_caixa'];
                    $product[$i]['peso_caixa'] = $form['peso_caixa'];
                    $product[$i]['container'] = $form['container'];
                    $product[$i]['lang'] = 'es';

                }

            }

            $category = new Categories();

            $entity = "";
            // Fazer inserção do produto
            foreach ($product as $item) {
                $entity = $this->model->create($item);
            }

            // if (isset($form['categories']) > 0){
            //     foreach ($form['categories'] as $category) {
            //         $entity->categories()->attach($category);
            //     }
            // }

            if ($entity) {

                $res = [
                    'status' => 200,
                    'data' => $entity,
                ];

            } else {
                $res = [
                    'status' => 500,
                    'data' => $entity,
                ];
            }

        } else {

            //Atualizar o registro
            $entity = $this->model->find($id);

            //Gera a url amigável
            $form['url'] = $this->url_verify($form['title'], $this->model, $id);

            if ($entity->update($form)) {
                if(isset($form['base64']) && $form['base64']!="") {
                    $update['image'] = $this->saveImg($form['base64'], 'produto_', '/img/products/', $entity->image);
                    $entity->update($update);
                }
            }

            if (isset($form['categories']) > 0) {
                //Remove todas as categorias e adiciona novamente
                $entity->categories()->detach();
                //Adiciona novamente as categorias
                foreach ($form['categories'] as $category) {
                    $entity->categories()->attach($category);
                }
            }

            $res = [
                'status' => 200,
                'data' => $entity,
            ];
        }

        // return response()->json($res);
    }

    public function delete(Request $request) {

        $id = $request->route('id');
        $entity = $this->model->find($id);

        // Excluir categorias
        $categories = $entity->categories()->detach();

        if ($entity->delete()) {
            @unlink(public_path() . '/img/products/' . $entity->image);
        }
    }
}
