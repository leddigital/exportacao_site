<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Contents;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    protected $model;

    public function __construct()
    {
        $this->model = new Categories();
    }

    public function index(Request $request)
    {
        return view(
            'admin.categories.index'
        );
    }

    //Listar todas as categorias
    public function readAll(Request $request)
    {
        $collection = $this->model->get()->all();
        $data['data'] = $collection;
        echo json_encode($data);
    }


    public function form(Request $request)
    {
        $id = $request->route('id');

        if (isset($id) and ($id != "")) {
            $entity = $this->model->where('group', '=', $id)->get();

            return view('admin.categories.form', ['entity' => $entity]);
        } else {
            return view('admin.categories.form');
        }
    }

    public function save(Request $request)
    {

        $form = $request->all();
        $id = $request->route('id');
        $destination_path = public_path() . '/categories';

        if(!isset($destination_path)){
            mkdir($destination_path, 0777);
        }

        if(isset($form['base64']) && ($form['base64'] != "") && !isset($id) and $id == "") {

            $form['image'] = $this->saveImg($form['base64'], 'categoria_' ,'/img/categories/');

            for ($i=0; $i < count($form['title']); $i++) {
                $form['url'][] = $this->url_verify($form['title'][$i], $this->model);

                if ($i == 0) {
                    $form['lang'][] = "en";
                } else if ($i == 1) {
                    $form['lang'][] = "es";
                }
            }

            for ($i=0; $i < count($form['title']); $i++) {

                if ($i == 0) {
                    $english['title'] = $form['title'][$i];
                    $english['short_description'] = $form['short_description'][$i];
                    $english['url'] = $form['url'][$i];
                    $english['image'] = $form['image'];
                    $english['lang'] = $form['lang'][$i];

                } else if ($i == 1) {
                    $spanish['title'] = $form['title'][$i];
                    $spanish['short_description'] = $form['short_description'][$i];
                    $spanish['url'] = $form['url'][$i];
                    $spanish['image'] = $form['image'];
                    $spanish['lang'] = $form['lang'][$i];

                }

            }

            // Fazer inserção da categoria
            $entity = $this->model->create($english);
            $entity = $this->model->create($spanish);

            $res = [
                'status' => 200,
                'data' => $entity
            ];

        }
        else {

            //Fazer update do registro
            // $entity = $this->model->find($id);

            // //Validação de URL
            // $form['url'] = $this->url_verify($form['title'], $this->model, $entity->id);

            // if(isset($form['base64'])) {
            //     $form['image'] = $this->saveImg($form['base64'], 'categoria_' ,'/img/categories/', $entity->image);
            // }

            // $entity = $entity->update($form);
            // $res = [
            //     'status' => 200,
            //     'data' => $entity
            // ];
        }
        return response()->json($res);
    }

    public function delete(Request $request){

        $destination = public_path() . '/img/categories/';
        $id = $request->route('id');
        $entity = $this->model->find($id);
        @unlink($destination . $entity->image);
        $entity->delete();
    }

}
