<header>
    <div class="container-fluid topbar">
        <div class="row">
            <div class="col-md-11 col-sm-12">
                <a href="{{ route('nav.index') }}">
                    <img class="logo" src="{{ asset('img/logo.png') }}" alt="">
                </a>
                <div class="language-select float-right">
                    <ul class="list-inline">
                        <li class="list-inline-item">
                            <a href="javascript:;"><img src="{{ asset('img/es_ES.png') }}" alt=""></a>
                        </li>
                        <li class="list-inline-item">
                            <a href="javascript:;"><img src="{{ asset('img/en_US.png') }}" alt=""></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <nav class="navbar navbar-expand-lg">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas fa-bars" style="color:#000; font-size:28px;"></i>
        </button>
        <div class="container">
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link {{ Route::is('nav.index')?'active':'' }}" href="{{ route('nav.index') }}">@lang('site.home')</a>
                    </li>
                    <li class="nav-item">
                        <a id="lista" class="nav-link" href="{{ route('nav.index') }}#itens">@lang('site.products')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ Route::is('nav.contato')?'active':'' }}" href="{{ route('nav.contato') }}">@lang('site.distribution')</a>
                    </li>
                    <li class="nav-item">
                        <a id="contato" class="nav-link" href="{{ route('nav.contato') }}#formulario">@lang('site.contact')</a>
                    </li>
                    {{-- <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            {{  Config::get('app.locale')=='es'?'Español':'English'  }}
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">
                                <img src="{{ asset('img/es.png') }}"
                                    class="img-fluid {{ Config::get('app.locale')=='es'?'selected':'' }}" /> Español
                            </a>
                            <a class="dropdown-item" href="#">
                                <img src="{{ asset('img/us.png') }}"
                                    class="img-fluid {{ Config::get('app.locale')=='us'?'selected':'' }}" /> English
                            </a>
                        </div>
                    </li> --}}

                </ul>
                <form action="{{ route('nav.procurar') }}" id="search-form" method="get" class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" name="s" type="search" placeholder="@lang('site.search')..." required
                        aria-label="Search">
                    <button class="btn btn-default my-2 my-sm-0" type="submit">@lang('site.search')</button>
                </form>
            </div>
        </div>
    </nav>
</header>
