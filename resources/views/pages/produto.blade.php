@extends('layouts.default')


@section('social-tags')

    <meta property="og:title" content="Kinino Export">
    <meta property="og:description" content="{{ $produto->title }}">
    <meta property="og:image" content="{{ asset('img/images/prod/'.$produto->image) }}">
    <meta property="og:image:alt" content="{{ asset('img/logo.png') }}">

    <meta property="og:image:width" content="1920"/>
    <meta property="og:image:height" content="820"/>
    <meta property="og:url" content="{{ route('nav.index') }}">


    <meta name="twitter:title" content="Kinino Export">
    <meta name="twitter:description" content="{{ $produto->title }}">
    <meta name="twitter:image"content="{{ asset('img/images/prod/'.$produto->image) }}">
    <meta name="twitter:card" content="summary_large_image">

@endsection

@section('scripts')
{{-- <script>
    var image_change;
    $(function(){
        $('ul.variations li a').on('click',function(e){
            e.preventDefault();
            image_change = $(this).data('image');
            $('#product-image-change').attr('src',image_change);
        });
    });
</script> --}}
@endsection

@section('content')

<section class="products">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('nav.index') }}">Home</a></li>
                        <li class="breadcrumb-item">
                            <a
                                href="{{ route('nav.produtos', ['categoria' => $categoria->url]) }}">{{ $categoria->title }}</a>
                        <li class="breadcrumb-item active" aria-current="page">{{ $produto->title }}</li>
                        </li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-6">
                <img id="product-image-change" src="{{ asset('img/images/prod/'.$produto->image) }}" class="img-fluid">
            </div>
            <div class="col-sm-12 col-md-12 col-lg-6 desc-produto">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="product-title">{{ $produto->title }}</h4>
                    </div>
                </div>

                <div><strong>Cod.: </strong>{{ $produto->sku }}</div>
                <div><strong>Validade Mês: </strong>{{ $produto->validade }}</div>
                <div><strong>Peso Unitário: </strong>{{ $produto->peso_unitario }}</div>
                <div><strong>NCM: </strong>{{ $produto->ncm }}</div>
                <div><strong>Transporte: </strong>{{ $produto->container }}</div>
                <div><strong>Unidades por caixa: </strong>{{ $produto->qtd_caixa }}</div>
                <div><strong>Dimensões caixa (CLA): </strong>{{ $produto->cla }}</div>
                <div><strong>M3 Caixa: </strong>{{ $produto->m3_caixa }}</div>
                <div><strong>Peso bruto com a caixa: </strong>{{ $produto->peso_caixa }}</div>

            </div>
        </div>
    </div>
</section>

@endsection
