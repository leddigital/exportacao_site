@extends('layouts.default')

@section('social-tags')

    <meta property="og:title" content="Kinino Export">
    <meta property="og:description" content="{{ $informations->meta_description }}">
    <meta property="og:image" content="{{ asset('img/banners/banner_5de9582e0af52.jpeg') }}">
    <meta property="og:image:alt" content="{{ asset('img/logo.png') }}">

    <meta property="og:image:width" content="1920"/>
    <meta property="og:image:height" content="820"/>
    <meta property="og:url" content="{{ route('nav.index') }}">


    <meta name="twitter:title" content="Kinino Export">
    <meta name="twitter:description" content="{{ $informations->meta_description }}">
    <meta name="twitter:image"content="{{ asset('img/banners/banner_5de9582e0af52.jpeg') }}">
    <meta name="twitter:card" content="summary_large_image">

@endsection

@section('css')
<link rel="stylesheet" type="text/css" href="{{ asset("plugins/Nivo-Slider-jQuery-master/nivo-slider.css") }}"
    title="bootstrap">
@endsection

@section('scripts')
<script src="{{ asset("plugins/Nivo-Slider-jQuery-master/jquery.nivo.slider.js") }}"></script>
@endsection

@section('content')
<section id="distribution">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <h3>@lang('distributor.title')</h3>
                @lang('distributor.text')
            </div>
            <div class="col-lg-6">
                <div class="row">
                    <div class="col-sm-12">
                        <h3>@lang('distributor.raiz-title')</h3>
                        <div class="row">
                            <div class="col-md-4 col-6">
                                <h4>Waelte Ferraz</h4>
                                <p>Raíz Brasil - Eirilar</p>
                                <p><a href="tel:+551732127404"><i class="fa fa-phone" aria-hidden="true"></i> +55
                                        173212-7404</a></p>
                                <p><a href="https://wa.me/+5517996571818" target="_blank"><i class="fab fa-whatsapp"
                                            aria-hidden="true"></i> +55 17 99657-1818</a></p>
                                <p>Brazil</p>
                            </div>
                            <div class="col-md-4 col-6">
                                <img src="{{ asset('img/waelte_rosto.jpg') }}" class="img-fluid" alt="">
                            </div>
                            <div class="col-md-4 col-12 text-center">
                                <a href="http://rbrasil.com.br" target="_blank"><img
                                        src="{{ asset('img/logo_raiz.jpg') }}" class="img-fluid" alt=""></a>
                            </div>
                        </div>

                    </div>
                    {{-- <div class="col-sm-12">
                        <div class="slider-wrapper theme-default">
                            <div id="slider" class="nivoSlider">
                                <img src="{{ asset('img/company.jpg') }}" data-thumb="{{ asset('img/company.jpg') }}"
                                    alt="" class="img-fluid">
                                <img src="{{ asset('img/company-1.jpg') }}"
                                    data-thumb="{{ asset('img/company-1.jpg') }}" alt="" class="img-fluid">
                            </div>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
</section>

<a id="formulario"></a>
<section id="contact-form" class="pt-5 pb-5">
        <div class="container-fluid">
            <div class="row justify-content-md-center">
                <div class="col-sm-8 col-md-6">
                    <h3 style="border-bottom:1px solid #DDD; padding-bottom:10px;text-align:center;">@lang('contact.title')
                    </h3>
                    <form id="contact" method="post">
                        {!! csrf_field() !!}
                        <div class="form-group">
                            <label for="name">@lang('contact.name')</label>
                            <input type="text" name="nome" class="form-control" id="name"
                                required>
                        </div>
                        <div class="form-group">
                            <label for="email">@lang('contact.email')</label>
                            <input type="email" name="email" class="form-control" id="email"
                                required>
                        </div>
                        <div class="form-group">
                            <label for="company">@lang('contact.company')</label>
                            <input type="text" name="empresa" class="form-control" id="company"
                                required>
                        </div>
                        <div class="form-group">
                            <label for="subject">@lang('contact.subject')</label>
                            <input type="text" name="assunto" class="form-control" id="subject"
                                required>
                        </div>
                        <div class="form-group">
                            <label for="subject">@lang('contact.message')</label>
                            <textarea class="form-control" name="mensagem" id="message" rows='5'
                                required></textarea>
                        </div>
                        <div class="form-group">
                            <button id="btn-contact-enviar" type="submit"
                                class="btn btn-block btn-default">@lang('contact.send')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

{{-- <section id="contact-form" class="pt-5 pb-5">
    <div class="container">
        <div class="form-content p-5 bg-white">
            <div class="row">

                <div class="col-md-4">

                    <div class="contact-info">
                        <h3>Fale Conosco</h3>

                        <div class="info-holder mb-3">
                            <span class="icon mr-3">
                                <i class="fa fa-phone icon" aria-hidden="true"></i>
                            </span>
                            <div class="info-text d-inline">
                                <a href="tel:+551732127404">
                                    +55 173212-7404
                                </a>
                            </div>
                        </div>

                        <div class="info-holder mb-3">
                            <span class="icon mr-3">
                                <i class="fab fa-whatsapp" aria-hidden="true"></i>
                            </span>
                            <div class="info-text d-inline">
                                <a href="https://wa.me/+5517996571818" target="_blank">
                                    +55 17 99657-1818
                                </a>
                            </div>
                        </div>

                        <div class="info-holder mb-3">
                            <span class="icon mr-3">
                                <i class="fa fa-envelope icon" aria-hidden="true"></i>
                            </span>
                            <div class="info-text d-inline">
                                <a href="mailto:waelte@raizbrasil.global" target="_blank">
                                    waelte@raizbrasil.global
                                </a>
                            </div>
                        </div>

                        <h3 class="mt-5">Nossa Localização</h3>

                        <div class="info-holder mb-3">
                            <p>R. Santo André, 385</p>
                            <p>Vila Santa Cruz</p>
                            <p>São José do Rio Preto - SP</p>
                            <p>15014-490</p>
                            <p>Brasil</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">

                </div>
            </div>
        </div>
    </div>
</section> --}}
<script>
    $(document).ready(function () {
        $('#contact').submit(function () {
            var serialize = $(this).serialize();
            $.ajax({
                method: 'POST',
                url: "{{ route('send.mail') }}",
                data: serialize,
                beforeSend: function () {
                    $('#btn-contact-enviar').attr('disabled', true);
                    $('#btn-contact-enviar').html('Enviando...');
                },
            }).done(function (response) {
                if (response == '1') {
                    Swal.fire({
                        type: 'success',
                        title: 'Enviado!',
                        text: 'E-mail enviado com sucesso!',
                    });
                    $('#contact')[0].reset();
                    $('#btn-contact-enviar').attr('disabled', false);
                    $('#btn-contact-enviar').html('ENVIAR');
                } else {
                    Swal.fire({
                        type: 'error',
                        title: 'Oops...',
                        text: 'Ocorreu algum erro ao enviar o seu contato.',
                    });
                    $('#contact')[0].reset();
                    $('#btn-contact-enviar').attr('disabled', false);
                    $('#btn-contact-enviar').html('ENVIAR');
                }
            });
            return false;
        });
    });

</script>
<script type="text/javascript">
    $(window).on('load', function () {
        $('#slider').nivoSlider({
            directionNav: true,
            controlNav: false,
            directionNav: true,
            pauseOnHover: true,
            animSpeed: 500,
            pauseTime: 6000,
            prevText: '<h1 style="color:#fff;font-size: 20px; padding-left: 10px;"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></h1>',
            nextText: '<h1 style="color:#fff;font-size: 20px; padding-right: 10px;"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></h1>',
        });
    });

</script>
@endsection
