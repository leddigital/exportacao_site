@extends('layouts.admin')
@section('title', 'Categoria')
@section('content')
<header class="page-header">
    <h2>Categoria</h2>
    <div class="right-wrapper text-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('information.index') }}">
                    <i class="fas fa-home"></i>
                </a>
            </li>
            <li><span>Cadastro de Categoria</span></li>
        </ol>
        <a class="sidebar-right-toggle" data-open=""><i class="fas fa-chevron-left"></i></a>
    </div>
</header>
<div class="row">
    <div class="col">
        <form class="form-horizontal form-bordered" id="form_cadastre" method="post"
            action="{{ isset($entity->id)?route('categories.edit.save',['id'=>$entity->id]):route('categories.save') }}"
            data-reload="{{ route('categories.index') }}">
            @csrf

            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                    </div>
                    <h2 class="card-title">Idioma</h2>
                    <p class="card-subtitle">
                        Selecione o idioma a no qual a categoria será cadastrada. Lembre-se que ambas os idiomas devem ser preenchidos.
                    </p>
                </header>
                <div class="card-body" style="display: block;">
                    <div class="form-group row d-flex justify-content-center">
                        <div class="col-lg-3">
                            <div class="form-group">

                                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="pills-ingles"
                                            data-toggle="pill" href="#pills-home" role="tab"
                                            aria-controls="pills-home" aria-selected="true">INGLÊS</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-espanhol"
                                            data-toggle="pill" href="#pills-profile" role="tab"
                                            aria-controls="pills-profile" aria-selected="false">ESPANHOL</a>
                                    </li>
                                </ul>

                            <div>
                        </div>
                    </div>
                </div>
            </section>


              <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-ingles">

                    <section class="card">
                        <header class="card-header">
                            <div class="card-actions">
                                <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                            </div>
                            <h2 class="card-title">Cadastro de Categorias de produtos</h2>
                            <p class="card-subtitle">
                                Preencha o título da categoria. Ex: Cadeiras, Poltronas, Mesas etc...
                            </p>
                        </header>
                        <div class="card-body" style="display: block;">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group row">
                                        <div class="col-lg-12">
                                            <div class="input-group">
                                                <span class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="fas fa-font"></i>
                                                    </span>
                                                </span>
                                                <input value="{{ isset($entity->title)?$entity->title:'' }}" type="text"
                                                    name="title[]" class="form-control" placeholder="Título" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="card">
                        <header class="card-header">
                            <div class="card-actions">
                                <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                            </div>
                            <h2 class="card-title">Descrição</h2>
                            <p class="card-subtitle">
                                Texto que irá aparecer abaixo da imagem principal. Deve conter uma breve descrição acerca do conteúdo.
                            </p>
                        </header>
                        <div class="card-body" style="display: block;">
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <textarea name="short_description[]'" class="form-control" row="3" required>{{ isset($entity->short_description)?$entity->short_description:'' }}</textarea>
                                    <div>
                                </div>
                            </div>
                        </div>
                    </section>

                </div>
                <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-espanhol">

                    <section class="card">
                        <header class="card-header">
                            <div class="card-actions">
                                <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                            </div>
                            <h2 class="card-title">Cadastro de Categorias de produtos</h2>
                            <p class="card-subtitle">
                                Preencha o título da categoria. Ex: Cadeiras, Poltronas, Mesas etc...
                            </p>
                        </header>
                        <div class="card-body" style="display: block;">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group row">
                                        <div class="col-lg-12">
                                            <div class="input-group">
                                                <span class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="fas fa-font"></i>
                                                    </span>
                                                </span>
                                                <input value="{{ isset($entity->title)?$entity->title:'' }}" type="text"
                                                    name="title[]'" class="form-control" placeholder="Título" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="card">
                        <header class="card-header">
                            <div class="card-actions">
                                <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                            </div>
                            <h2 class="card-title">Descrição</h2>
                            <p class="card-subtitle">
                                Texto que irá aparecer abaixo da imagem principal. Deve conter uma breve descrição acerca do conteúdo.
                            </p>
                        </header>
                        <div class="card-body" style="display: block;">
                            <div class="form-group row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <textarea name="short_description[]" class="form-control" row="3" required>{{ isset($entity->short_description)?$entity->short_description:'' }}</textarea>
                                    <div>
                                </div>
                            </div>
                        </div>
                    </section>

                </div>
              </div>

            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                    </div>
                    <h2 class="card-title">Imagem Principal</h2>
                    <p class="card-subtitle">
                        Selecione uma imagem como imagem principal.<br />
                        <br />
                        <strong>Obs.: A imagem selecionada será automatimacamente reajustada para o tamanho na descrição
                            do campo.</strong>
                    </p>
                </header>
                <div class="card-body" style="display: block;">
                    <div class="row">
                        <div class="col-lg-12">
                            <small>Tamanho da imagem 175px x 175px</small>
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="input-append">
                                    <div class="uneditable-input">
                                        <i class="fas fa-file fileupload-exists"></i>
                                        <span class="fileupload-preview"></span>
                                    </div>
                                    <span class="btn btn-default btn-file">
                                        <span class="fileupload-exists">Trocar</span>
                                        <span class="fileupload-new">Selecionar Imagem</span>
                                        <input type="file" name='banner' accept="image/*" onchange='loadPreview(this, 175,175)'
                                            value="{{ isset($entity->image)!=""?"/img/categories/".$entity->banners:'' }}">
                                    </span>
                                    <a href="javascript:;" class="btn btn-default fileupload-exists"
                                        data-dismiss="fileupload">Remove</a>
                                    @if (isset($entity->image)!="")
                                    <a href="javascript:;" class="btn btn-default" id="remove_image_default">Remove</a>
                                    @endif
                                    <input type="hidden" name="remove_image_default" />
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <textarea id="base64" name="base64" style="display:none;"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="img-content">
                                <img id='output' class='img-fluid'
                                    src='{{ isset($entity->image)!=""?"/img/categories/".$entity->image:'' }}'>
                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <section class="card">
                <div class="card-body" style="display: block;">
                    <button type="submit" class="mb-1 mt-1 mr-1 btn btn-success"><i class="fas fa-save"></i>
                        Salvar</button>
                </div>
            </section>
        </form>
    </div>
</div>
@endsection
