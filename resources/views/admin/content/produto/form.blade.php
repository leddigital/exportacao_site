@extends('layouts.admin')
@section('title', 'Cadastrar novo produto')
@section('content')

<header class="page-header">
    <h2>Produto</h2>
    <div class="right-wrapper text-right">
        <ol class="breadcrumbs">
            <li>
                <a href="{{ route('information.index') }}">
                    <i class="fas fa-home"></i>
                </a>
            </li>
            <li><span>Produto</span></li>
        </ol>
        <a class="sidebar-right-toggle" data-open=""><i class="fas fa-chevron-left"></i></a>
    </div>
</header>
<div class="row">
    <div class="col">
        <form class="form-horizontal form-bordered" id="form_cadastre" method="post"
            action="{{ isset($entity->id)?route('content.edit.save',['id'=>$entity->id]):route('content.save') }}"
            data-reload="{{ route('content.index',['_type'=>'produto']) }}">
            @csrf
            <input type="hidden" name="type" value="{{ $_type }}">

            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                    </div>
                    <h2 class="card-title">Idioma</h2>
                    <p class="card-subtitle">
                        Selecione o idioma a no qual a categoria será cadastrada. Lembre-se que ambas os idiomas devem
                        ser preenchidos.
                    </p>
                </header>
                <div class="card-body" style="display: block;">
                    <div class="form-group row d-flex justify-content-center">
                        <div class="col-lg-3">
                            <div class="form-group">

                                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="pills-ingles" data-toggle="pill"
                                            href="#pills-home" role="tab" aria-controls="pills-home"
                                            aria-selected="true">INGLÊS</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="pills-espanhol" data-toggle="pill" href="#pills-profile"
                                            role="tab" aria-controls="pills-profile" aria-selected="false">ESPANHOL</a>
                                    </li>
                                </ul>

                                <div>
                                </div>
                            </div>
                        </div>
            </section>

            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                    </div>

                    <h2 class="card-title">Informações básicas</h2>
                    <p class="card-subtitle">
                        Informações que serão utilizadas no SEO do site.
                    </p>
                </header>
                <div class="card-body" style="display: block;">
                    <div class="row">
                        <div class="col-lg-12">

                            <div class="tab-content tab-content-product mb-3" id="pills-tabContent">

                                <div class="tab-pane fade show active" id="pills-home" role="tabpanel"
                                    aria-labelledby="pills-ingles">
                                    <div class="form-group row">
                                        <div class="col-lg-12">
                                            <label for="title-eng">Título em INGLÊS</label>
                                            <div class="input-group">
                                                <span class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="fas fa-align-left"></i>
                                                    </span>
                                                </span>

                                                <input id="title-eng"
                                                    value="{{ isset($entity->title)!=""?$entity->title:'' }}"
                                                    type="text" name="title[]" class="form-control" placeholder="Titulo"
                                                    required>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane fade" id="pills-profile" role="tabpanel"
                                    aria-labelledby="pills-espanhol">
                                    <div class="form-group row">
                                        <div class="col-lg-12">
                                            <label for="title-esp">Título em ESPANHOL</label>
                                            <div class="input-group">
                                                <span class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="fas fa-align-left"></i>
                                                    </span>
                                                </span>

                                                <input id="title-esp"
                                                    value="{{ isset($entity->title)!=""?$entity->title:'' }}"
                                                    type="text" name="title[]" class="form-control" placeholder="Titulo"
                                                    required>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="form-group row">
                                <label class="col-lg-3 control-label text-lg-right pt-2">Categorias</label>
                                <div class="col-lg-6">
                                    <select name="categories[]" multiple data-plugin-selectTwo
                                        class="form-control populate" id="select_categories">
                                        @foreach ($categories as $category)
                                        <option value="{{ $category->id }}">
                                            {{ $category->title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>

            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                    </div>
                    <h2 class="card-title">Informações técnicas</h2>
                    <p class="card-subtitle">
                        Informe as informações técnicas que serão exibidas no site.
                    </p>
                </header>
                <div class="card-body" style="display: block;">
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col-lg-2">
                                        <label for="sku">CÓD. INT</label>
                                        <input type="text" class="form-control" id="sku" name="sku"
                                            value="{{ isset($entity->sku)!=""?$entity->sku:"" }}">
                                    </div>
                                    <div class="col-lg-3">
                                        <label for="ncm">NCM</label>
                                        <input type="text" class="form-control" id="ncm" name="ncm"
                                            value="{{ isset($entity->ncm)!=""?$entity->ncm:"" }}">
                                    </div>
                                    <div class="col-lg-2">
                                        <label for="validade">VALIDADE MÊS</label>
                                        <input type="text" class="form-control" id="validade" name="validade"
                                            value="{{ isset($entity->validade)!=""?$entity->validade:"" }}">
                                    </div>
                                    <div class="col-lg-3">
                                        <label for="peso_unitario">PESO UNITARIO</label>
                                        <input type="text" class="form-control" id="peso_unitario" name="peso_unitario"
                                            value="{{ isset($entity->peso_unitario)!=""?$entity->peso_unitario:"" }}">
                                    </div>
                                    <div class="col-lg-2">
                                        <label for="container">ARMAZENAMENTO</label>
                                        <select class="form-control" id="container" name="container">
                                            <option
                                                {{ (isset($entity->container) and $entity->container == "caixa")?"selected":"" }}
                                                value="caixa">Caixa</option>
                                            <option
                                                {{ (isset($entity->container) and $entity->container == "pallet")?"selected":"" }}
                                                value="pallet">Pallet</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12 d-flex justify-content-center">
                            <div class="form-group">
                                <div class="form-row">
                                    <div class="col-lg-3">
                                        <label for="qtd_caixa">UNIDADES POR CAIXA</label>
                                        <input type="text" class="form-control" id="qtd_caixa" name="qtd_caixa"
                                            value="{{ isset($entity->qtd_caixa)!=""?$entity->qtd_caixa:"" }}">
                                    </div>
                                    <div class="col-lg-3">
                                        <label for="cla">DIMENSOES CAIXA (CLA)</label>
                                        <input type="text" class="form-control" id="cla" name="cla"
                                            value="{{ isset($entity->cla)!=""?$entity->cla:"" }}">
                                    </div>
                                    <div class="col-lg-2">
                                        <label for="m3_caixa">M3 CAIXA</label>
                                        <input type="text" class="form-control" id="m3_caixa" name="m3_caixa"
                                            value="{{ isset($entity->m3_caixa)!=""?$entity->m3_caixa:"" }}">
                                    </div>
                                    <div class="col-lg-3">
                                        <label for="peso_caixa">PESO BRUTO COM A CAIXA</label>
                                        <input type="text" class="form-control" id="peso_caixa" name="peso_caixa"
                                            value="{{ isset($entity->peso_caixa)!=""?$entity->peso_caixa:"" }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="card">
                <header class="card-header">
                    <div class="card-actions">
                        <a href="forms-basic.html#" class="card-action card-action-toggle" data-card-toggle=""></a>
                    </div>
                    <h2 class="card-title">Imagem Principal</h2>
                    <p class="card-subtitle">
                        Selecione uma imagem como imagem principal.<br />
                        <br />
                        <strong>Obs.: A imagem selecionada será automatimacamente reajustada para o tamanho na descrição
                            do campo.</strong>
                    </p>
                </header>
                <div class="card-body" style="display: block;">
                    <div class="row">
                        <div class="col-lg-12">
                            <small>Tamanho da imagem 370px x 280px</small>
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="input-append">
                                    <div class="uneditable-input">
                                        <i class="fas fa-file fileupload-exists"></i>
                                        <span class="fileupload-preview"></span>
                                    </div>
                                    <span class="btn btn-default btn-file">
                                        <span class="fileupload-exists">Trocar</span>
                                        <span class="fileupload-new">Selecionar Imagem</span>
                                        <input type="file" name='banner' accept="image/*"
                                            onchange='loadPreview(this, 1024,768)'
                                            value="{{ isset($entity->image)!=""?"/img/products/".$entity->image:'' }}">
                                    </span>
                                    <a href="javascript:;" class="btn btn-default fileupload-exists"
                                        data-dismiss="fileupload">Remove</a>
                                    @if (isset($entity->image)!="")
                                    <a href="javascript:;" class="btn btn-default" id="remove_image_default">Remove</a>
                                    @endif
                                    <input type="hidden" name="remove_image_default" />
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <textarea id="base64" name="base64" style="display:none;"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="img-content">
                                <img id='output' class='img-fluid'
                                    src='{{ isset($entity->image)!=""?"/img/products/".$entity->image:'' }}'>
                            </div>
                        </div>
                    </div>
                </div>
            </section>



            <section class="card">
                <div class="card-body" style="display: block;">
                    <button type="submit" class="mb-1 mt-1 mr-1 btn btn-success"><i class="fas fa-save"></i>
                        Salvar</button>
                </div>
            </section>
        </form>
    </div>
</div>


<script id="delete_gallery_template" type="x-tmpl-mustache">
    <input type="text" name="delete_gallery[]" class="form-control input-sm" placeholder="" value=" "/>
</script>
<script id="thumb_template" type="x-tmpl-mustache">
    <div class="gallery_item">
        <img src="@{{ thumb }}" class="img-responsive"/>
        <input type="hidden" name="gallery_image[]" value="@{{ original }}" />
        <input type="hidden" name="gallery_image_thumb[]" value="@{{ thumb }}" />
        <input type="text" name="gallery_image_description[]" class="form-control input-sm" placeholder="Descrição da imagem"/>
        <a href="javascript:;" class="" id="gallery_item_remove">
            <i class="mdi mdi-delete"></i>Remover
        </a>
    </div>
</script>

@endsection
