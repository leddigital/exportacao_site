<?php

return [
    'title'=>'Entre en contacto',
    'name' => 'Nombre',
    'email' => 'Correo electrónico',
    'company' => 'Compañia',
    'subject' => 'Asunto',
    'message' => 'Mensaje',
    'send'=>'Enviar'
];
