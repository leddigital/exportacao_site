<?php

return [
    'raiz-title' => 'Departamento de Exportaciones',
    'title' => 'Sea un distribuidor',
    'text' => '
    <p>
       Somos uma empresa brasileira, fundada em 1995, em São José do Rio Preto, interior de São Paulo.
    </p>
    <p>
        Desde o início, acompanhamos o crescimento do mercado consumidor e atendemos às suas necessidades. Nosso portfólio de produtos segue em constante expansão e inovação, contando com mais de 300 itens de condimentos, especiarias, temperos, molhos, pimentas, farináceos, pipocas, chás, sobremesas, doces, grãos, alimentos para pássaros e produtos naturais. Sempre levando novidades para o mercado, os lançamentos são feitos após rigorosos processos de desenvolvimento e testes feitos por engenheiros de alimentos. 
    </p>
    <p>O nosso compromisso é com a qualidade. Isso nos faz líderes do segmento no interior de São Paulo e grande projeção e visibilidade no Brasil. Agora, estamos em um processo de expansão que colocará os nossos produtos no mercado internacional.</p>
    <p>
    Quem é distribuidor Kinino, tem um grande produto em mão para alavancar as vendas:
        <ul>
            <li>Linha diversificada de produtos;</li>
            <li>Suporte de marketing para aumentar as vendas;</li>
            <li>Contratos exclusivos com o local de destino da comercialização;</li>
            <li>Suporte especializado ao comércio exterior. </li>
        </ul>
    </p>
    <p>Seja um distribuidor Kinino.</p>
    <p>Aumente seu portfólio de vendas e fature mais.</p>
    <p>
        <strong>Teléfono:</strong> 0800 707 9220<br />
        <strong>Email:</strong> <a href="mailto:vendas@eirilar.com.br">vendas@eirilar.com.br</a>
    </p>
    ',
    'company' => 'Company',
    'subject' => 'Subject',
    'message' => 'Message',
    'send' => 'Send'
];


