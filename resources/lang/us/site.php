<?php

return [
    'home' => 'Home',
    'search' => 'Search',
    'products' => 'Products',
    'distribution' => 'Become a Distributor',
    'contact' => 'Contact'
];
